//  importing file-Reader and fast-csv
const fs = require('fs');
const csv = require('fast-csv');

// creating variable in global space
const matchesBySeason = {};

// parsing rows of csv file 
fs
	.createReadStream('matches.csv')
	.pipe(csv.parse({ headers: true }))
	.on('data', row => countMatchPlayedPerYear(row))
	.on('end', row => {
		console.log(matchesBySeason);
	});

// function to countMatchPlayedPerYear 
function countMatchPlayedPerYear(row) {
	season = row['season'];
	if (season in matchesBySeason) {
		matchesBySeason[season] = matchesBySeason[season] + 1;
	} else {
		matchesBySeason[season] = 1;
	}
}