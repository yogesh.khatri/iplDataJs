//  importing file-Reader and fast-csv
const fs = require('fs');
const csv = require('fast-csv');

// creating variable in global space
const matchesWonByTeamsInEachSeason = {};

// parsing rows of csv file
fs
	.createReadStream('matches.csv')
	.pipe(csv.parse({ headers: true }))
	.on('data', row => countingMatchesWonByTeamsInEachSeason(row))
	.on('end', row => {
		console.log(matchesWonByTeamsInEachSeason);
	});

// function to countMatchPlayedPerYear
function countingMatchesWonByTeamsInEachSeason(row) {
	var season = row['season'];
	var winner = row['winner'];
	if (season in matchesWonByTeamsInEachSeason) {
		if (winner in matchesWonByTeamsInEachSeason[season]) {
			matchesWonByTeamsInEachSeason[season][winner] = matchesWonByTeamsInEachSeason[season][winner] + 1;
		} else {
			matchesWonByTeamsInEachSeason[season][winner] = 1;
		}
	} else {
		matchesWonByTeamsInEachSeason[season] = {};
		matchesWonByTeamsInEachSeason[season][winner] = 1;
	}
}
