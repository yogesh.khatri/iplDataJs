// importing file-Reader and fast-csv
const fs = require('fs');
const csv = require('fast-csv');

// declaring array and object in global space
const matchIdOf2015 = [];
const numberOfBallsAndRuns = {};
const economyOfPlayer = {};

// reading and parsing matches.csv file to get idOfSeason
fs
	.createReadStream('matches.csv')
	.pipe(csv.parse({ headers: true }))
	.on('data', row => idOfSeason(row));
// reading and parsing deliveries.csv to get countingBallsAndRuns
fs
	.createReadStream('deliveries.csv')
	.pipe(csv.parse({ headers: true }))
	.on('data', row => countingBallsAndRuns(row))
	.on('end', row => {
		calculatingEconomyOfPlayers(numberOfBallsAndRuns);
	});

// function to get id of 2016 season
function idOfSeason(row) {
	var season = row['season'];
	var id = row['id'];
	if (season == '2015') {
		matchIdOf2015.push(id);
	}
}

// function to calculate economyOfPlayer of players
function countingBallsAndRuns(row) {
	var match_id = row['match_id'];
	var bowler = row['bowler'];
	var runsGiven = row['total_runs'];
	if (match_id in matchIdOf2015) {
		if (bowler in numberOfBallsAndRuns) {
			numberOfBallsAndRuns[bowler][0] = numberOfBallsAndRuns[bowler][0] + parseInt(runsGiven);
			numberOfBallsAndRuns[bowler][1] = numberOfBallsAndRuns[bowler][1] + 1;
		} else {
			numberOfBallsAndRuns[bowler] = [];
			numberOfBallsAndRuns[bowler][0] = parseInt(runsGiven);
			numberOfBallsAndRuns[bowler][1] = 1;
		}
	}
}

// calculating economyOfPlayer of bowlers
function calculatingEconomyOfPlayers(numberOfBallsAndRuns) {
	for (let bowler in numberOfBallsAndRuns) {
		economyOfPlayer[bowler] = (numberOfBallsAndRuns[bowler][0] * 6 / numberOfBallsAndRuns[bowler][1]).toFixed(3);
	}
	var sortedEconomyOfPlayer = [];
	for (var bowler in economyOfPlayer) {
		sortedEconomyOfPlayer.push([bowler, economyOfPlayer[bowler]]);
	}
	sortedEconomyOfPlayer.sort(function(a, b) {
		return a[1] - b[1];
	});

	for (let i = 0; i < 10; i++) {
		console.log(sortedEconomyOfPlayer[i]);
	}
}
