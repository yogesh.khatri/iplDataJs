// importing file-Reader and fast-csv
const fs = require('fs');
const csv = require('fast-csv');

// declaring array and object in global space 
const matchIdOf2016 = [];
const extraRunsOfTeam = {};

// reading and parsing matches.csv file to get idOfSeason
fs.createReadStream('matches.csv')
	.pipe(csv.parse({ headers: true }))
	.on('data', row => idOfSeason(row));

// reading and parsing deliveries.csv to get extraRunsByTeamIn2016 
fs.createReadStream('deliveries.csv')
	.pipe(csv.parse({ headers: true }))
	.on('data', row => extraRunsByTeamIn2016(row))
	.on('end', row => {
		console.log(extraRunsOfTeam);
	});

// function to get id of 2016 season
function idOfSeason(row) {
	var season = row['season'];
	var id = row['id'];
	if (season == '2016') {
		matchIdOf2016.push(id);
	}
}
// function to get extraRuns By team in 2016
function extraRunsByTeamIn2016(row) {
	var batting_team = row['batting_team'];
	var extra_runs = row['extra_runs'];
	var match_id = row['match_id'];
	if (match_id in matchIdOf2016) {
		if (batting_team in extraRunsOfTeam) {
			extraRunsOfTeam[batting_team] = extraRunsOfTeam[batting_team] + parseInt(extra_runs);
		} else {
			extraRunsOfTeam[batting_team] = parseInt(extra_runs);
		}
	}
}
